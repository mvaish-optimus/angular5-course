import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  private isDetailsVisible = true;
  private logMessages = [];

  constructor() {
  }
  private onDisplayDetails() {
    this.isDetailsVisible = !this.isDetailsVisible;
    this.logMessages.push((this.logMessages.length)+1);
  }
  private getBgColor(value: number) {
    if (value >= 5) {
      return 'blue';
    }
    return 'transparent';
  }
}
